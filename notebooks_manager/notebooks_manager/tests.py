#coding: utf-8
'''
Created on 18-06-2013

@author: bambo
'''
from django.core.urlresolvers import resolve
from django.http.request import HttpRequest
from django.shortcuts import render
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.test import TestCase
from notebooks_manager.models import Notebook, Place
from notebooks_manager.views import addNotebook, homePage, addNotebookForm, \
    viewNotebooks

    
class TestHomePage(TestCase):
    
    def test_CanGetHomePage(self):
        request = HttpRequest()
        response = homePage(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content, expected_html)
        
    
class TestAddNotebook(TestCase):
   
   
    def test_CanResolveNotebookAddPage(self):
        request = HttpRequest()
        response = addNotebook(request)
        form = addNotebookForm()
          
        expected_html = render_to_string('addNotebook.html', {'addNotebookForm' : form,
                                                              } )   
        self.assertEqual(response.content, expected_html  )
          
    def test_CanHandlePOSTRequest(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['model'] = 'Dell'
        request.POST['serial'] = '0912-2aL'
        request.POST['localization'] = Place.objects.get(name="Krakow").id
        request.POST['status'] = 1
        request.POST['working_status'] = 1
        request.POST['notes'] = ""
        response = addNotebook(request)
        new_notebook = Notebook.objects.latest('pk')
        self.assertEqual(new_notebook.model, 'Dell')
        self.assertEqual(new_notebook.serial, '0912-2aL')
        self.assertEqual(new_notebook.localization, Place.objects.get(name="Krakow"))
           
        expected_html = render_to_string(
                                         'addNotebook.html',
                                         {'addNotebookForm' : addNotebookForm(),
                                          'confirmNotebook' : new_notebook})
        self.assertEqual(expected_html, response.content)
          
    def test_CanSaveTwoNotebooks(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['model'] = 'Dell'
        request.POST['serial'] = '0912-2aL'
        request.POST['localization'] = Place.objects.get(name="Krakow").id
        request.POST['status'] = 1
        request.POST['working_status'] = 1
          
        response = addNotebook(request)
          
        request = HttpRequest()
        request.method = 'POST'
        request.POST['model'] = 'Lenovo'
        request.POST['serial'] = '0912-2aL'
        request.POST['localization'] = Place.objects.get(name="Krakow").id
        request.POST['status'] = 1
        request.POST['working_status'] = 1
          
        response = addNotebook(request)
          
        new_notebook = Notebook.objects.latest('pk')
        self.assertEqual(new_notebook.model, 'Lenovo')
        self.assertEqual(new_notebook.serial, '0912-2aL')
        self.assertEqual(new_notebook.localization, Place.objects.get(name="Krakow"))
        expected_html = render_to_string(
                                         'addNotebook.html',
                                         {'addNotebookForm' : addNotebookForm(),
                                          'confirmNotebook' : new_notebook})
        self.assertEqual(expected_html, response.content)
         
class NotebookModelTest(TestCase):
     
    def test_savingAndRetrievingNotebook(self):
        notebook_1 = Notebook()
        notebook_1.model = 'Dell'
        notebook_1.serial = '091-12L'
        notebook_1.localization = Place.objects.get(name="Krakow")
        notebook_1.working = 1
        notebook_1.status = 1
        notebook_1.save()
        
        saved_items = Notebook.objects.all()
        self.assertEqual(saved_items.count(), 1);
        self.assertEqual(notebook_1, saved_items[0])
          
class viewNotebooksTest(TestCase):
     
    def test_CanResolveViewNotebooksPage(self):
        request = HttpRequest()
        response = viewNotebooks(request)
         
        expected_html = render_to_string('viewNotebooks.html', {'notebooks' : Notebook.objects.all()},
                                         context_instance = RequestContext(request))  
        self.assertEqual(expected_html, response.content)
     
         

