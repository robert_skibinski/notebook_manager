#coding: utf-8
from collections import defaultdict
from datetime import date, timedelta
import datetime
import json

from django.core import serializers
from django.forms.formsets import formset_factory
from django.http import response
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response, get_object_or_404, \
    redirect
from django.template.context import RequestContext
from django.utils import simplejson
from django_tables2.config import RequestConfig

from notebooks_manager.forms import addNotebookForm, addCourseForm, \
    addShipmentForm, DateForm
from notebooks_manager.models import Notebook, Shipment, Course, Place
from notebooks_manager.tables import NotebookTable, ShipmentTable, CourseTable, \
    NotebookEditTable, FinishedCourseTable, NotebookTableWOStatus, NotebookDashTable
from notebooks_manager.utils import next_weekday, get_or_none, \
    getNotebooksListOnDate, getNotebooksListBetweenDates, weekday, \
    getCoursesListBetweenDates, dashboard


def homePage(request):
    
    return render(request   , 'home.html')


def addNotebook(request, notebook_id = None):
    if ( notebook_id ):
        notebook_instance = Notebook.objects.get(pk = notebook_id or None)
    else:
        notebook_instance = None
    if request.method == 'POST':
        
        form = addNotebookForm(request.POST, instance=notebook_instance)
        if form.is_valid():
            notebook_instance = form.save()
            if(notebook_id):
                confirmMessage = "Pomyślnie zmodyfikowano "
            else:
                confirmMessage = "Pomyślnie dodano"
            confirmMessage += " notebooka %s." % notebook_instance.serial
            print confirmMessage
            return render(request, 'addNotebook.html',{
                                                       'addNotebookForm' : form,
                                                       'confirmMessage': confirmMessage})
            
    else:
        form = addNotebookForm(request.POST or None, instance = notebook_instance)
        return render(request, 'addNotebook.html',{
                                                'addNotebookForm': form,
                                                } )
    
    
def viewNotebooks(request):
#     d = datetime.date(2013,9,10)
    if request.method == 'POST':
#         print request.POST
        day = int(request.POST['date_day'])
        month = int(request.POST['date_month'])
        year = int(request.POST['date_year'])
        day1 = int(request.POST['date1_day'])
        month1 = int(request.POST['date1_month'])
        year1 = int(request.POST['date1_year'])
        date = datetime.date(year, month, day)
        date1 = datetime.date(year1,month1,day1)
    else:
        date = next_weekday(datetime.date.today(), 0)
        date1 = next_weekday(datetime.date.today(), 0) + timedelta(days=4)
    dateForm = DateForm(initial = {'date': date, 'date1': date1})
    notebooks_instances = getNotebooksListBetweenDates(date,date1)
    for n in notebooks_instances:
        notebooks = NotebookEditTable(notebooks_instances)
    notebookDashTable = NotebookDashTable(notebooks_instances).getHtml()
    
    
    if request.method == 'POST':
        return HttpResponse(simplejson.dumps({'notebooks':notebooks.as_html(),
                                               'dashTable': notebookDashTable}), mimetype="application/json")
    else:
        return render_to_response('viewNotebooks.html', {'notebooks' : notebooks, 'dateForm': dateForm, 
                                                         'dashTable': notebookDashTable},
                              context_instance = RequestContext(request))


def deleteNotebook(request, notebook_id=None):
    uri = request.META.get('HTTP_REFERER') or '/course/view'
    notebook = get_or_none(Notebook, pk=notebook_id)
    if notebook != None:
        notebook.delete()
    return redirect(uri)


def addCourse(request, course_id=None):
    """
    View is responsible for adding new Courses and its' shipments, or editing existing ones
    """

    
    if request.method == 'POST':
        if request.POST['key'] == 'addCourse':
            placePk = request.POST["localization"]
            place = get_or_none(Place, pk=placePk)
            
            if(place == None):
                #check for duplicate name
                place = get_or_none(Place, name=placePk)
#                 print place
                if(place == None):#no duplicated name - creating new place
                    place = Place(name = placePk)
                    place.save()
                POST = request.POST.copy()
                POST["localization"] = place.id
#                 print POST
                cf = addCourseForm(POST)
            else:
                cf = addCourseForm(request.POST)
            
            cf_valid = cf.is_valid()
#             print request.POST
            if cf_valid: 
                c = cf.save()
                nbks = request.POST.getlist("selection")
                nbks_objects = Notebook.objects.filter(pk__in=nbks)
#                 print nbks_objects
            
                shipment_places = defaultdict(list)
                
                for notebook in nbks_objects:
                    if notebook.status == Notebook.FREE:
                        c.notebooks.add(notebook)
                        notebook.save()
                        if notebook.localization != c.localization:
#                             print "Dodaje"
                            shipment_places[notebook.localization].append(notebook)
                    else:
                        pass#jakies jqeury sprawdzajace juz przy zaznaczaniu,albo na poziomie renderowania tabeli
                if shipment_places.__len__() != 0:
                    for k,v in shipment_places.iteritems():
                        sh = Shipment()
                        sh.shipment_date = c.start_date - timedelta(days=3)
                        sh.delivery_date = c.start_date - timedelta(days=1)
                        sh.shipment_to = c.localization
                        sh.shipment_from = k
                        sh.save()
                        c.shipments.add(sh)
                        c.save()
                        for notebook in v:
                            sh.notebooks.add(notebook)
                    destination = '/course/addShipment/%s/' % c.id
                else:
                    destination = "/course/view"
                return HttpResponse(simplejson.dumps({'destination': destination}))
            else:
                pass #form validation error handling
        elif request.POST['key'] == 'refresh':
            year1 = int(request.POST['start_date_year'])
            month1 = int(request.POST['start_date_month'])
            day1 = int(request.POST['start_date_day'])
            year2 = int(request.POST['end_date_year'])
            month2 = int(request.POST['end_date_month'])
            day2 = int(request.POST['end_date_day'])
            date1 = datetime.date(year1, month1, day1)
            date2 = datetime.date(year2, month2, day2)
            notebooks = NotebookTable(getNotebooksListBetweenDates(date1, date2))
            return HttpResponse(simplejson.dumps({'notebooks':notebooks.as_html()}), mimetype="application/json")
        
    else:
        next_monday = next_weekday(date.today(), 0)
        next_friday = next_monday + timedelta(days=4)
        c = Course()
        c.start_date = next_monday
        c.end_date = next_friday
        
        cf = addCourseForm(instance = c)
        notebookTable = NotebookTable(getNotebooksListBetweenDates(c.start_date, c.end_date))
        RequestConfig(request, paginate=False).configure(notebookTable)
        return render(request,'addCourse.html', {'notebooks': Notebook.objects.all(),
                                             'addCourseForm': cf,
                                             'notebookTable': notebookTable,
                                             'dashboard': dashboard(),
                                             })
        
        
def addCourseOnSuccess(request):
    pass


def addCourseOnFailure(request):
    pass


def viewCourses(request):
#     courseTable = CourseTable(Course.objects.all()) 
    courses = Course.objects.all()
    today = datetime.date.today()
    finished = [c for c in courses if c.end_date <= today]
    ongoing = [c for c in courses if c.start_date <= today and c.end_date > today]
    incoming = [c for c in courses if c.start_date > today]
    finishedTable = FinishedCourseTable(finished)
    ongoingTable = CourseTable(ongoing)
    incomingTable = CourseTable(incoming)
    return render(request,'viewCourses.html', {'ongoingTable': ongoingTable, 'finishedTable': finishedTable,
                                               'incomingTable': incomingTable})


def deleteCourse(request, course_id=None):
    uri = request.META.get('HTTP_REFERER') or '/course/view'
    course = get_or_none(Course, pk=course_id)
    if course != None:
        course.delete()
    return redirect(uri)

def confirmCourse(request, course_id=None):
    uri = request.META.get('HTTP_REFERER') or '/course/view'
    course = get_or_none(Course, pk=course_id)
    if course != None:
        course.course_status = Course.CONFIRMED
        course.save()
    for shipment in course.shipments.all():
        return_shipment = Shipment(shipment_date=course.end_date, delivery_date=course.end_date,
                                     shipment_from = course.localization, shipment_to = shipment.shipment_from) 
        return_shipment.save()
        return_shipment.notebooks = shipment.notebooks.all()
        course.shipments.add(return_shipment)
    course.save()
    return redirect(uri)   
    
def addShipment(request, shipment_id=None):
    
    if request.method == "POST":
        shipmentForm = addShipmentForm(request.POST, instance = get_or_none(Shipment, pk=shipment_id))
        if shipmentForm.is_valid():
            print "valid"
            shipmentForm.save()
        destination = '/shipment/view/'
        return HttpResponse(simplejson.dumps({'destination': destination}))
    
    else:
        shipmentForm = addShipmentForm(initial={'shipment_date': datetime.date.today(), 
                                            'delivery_date': datetime.date.today() + timedelta(days=3)})
        notebookTable = NotebookTable([i for i in getNotebooksListOnDate(datetime.date.today) if i.status == Notebook.FREE])
        return render(request, 'addShipment.html', {'shipmentForm' : shipmentForm, 'notebookTable': notebookTable})
    
    
def addShipmentToCourse(request, course_id=None):
    if request.method == "POST":
        print request.POST
        if request.POST['key'] == 'shipment':
            shipment_id = int(request.POST['shipment_id'].partition('"')[0])
            shipment_instance = get_object_or_404(Shipment, id=shipment_id)
            POST = request.POST.copy()  #That's fuckin ridicoulus workaround.
            #Trzeba zhackowac POSTa, te dwa pola są zablokowane i nie idą w requeście i formularz sie nie waliduje.
            POST['shipment_from'] = shipment_instance.shipment_from.id
            POST['shipment_to'] = shipment_instance.shipment_to.id
            sf = addShipmentForm(POST, instance=shipment_instance) #But it's working as hell
            if sf.is_valid():
                sf.save()
        elif request.POST['key'] == 'course':
            course_id = int(request.POST['course_id'].partition('"')[0])
            course_instance = get_object_or_404(Course, id=course_id)
            POST = request.POST.copy()
            POST['start_date'] = course_instance.start_date
            POST['end_date'] = course_instance.end_date
            POST['localization'] = course_instance.localization.id
            cf = addCourseForm(POST, instance=course_instance)
            if cf.is_valid():
                cf.save()
        elif request.POST['key'] == 'redirect':
            return HttpResponse(simplejson.dumps({'destination': '/course/view'}))
    else:
        course = Course.objects.get(pk = course_id)
        shipments = course.shipments.all().order_by('-shipment_date')
         
        courseForm = addCourseForm(instance=course, initial={'course_id': course.id,})
        courseForm.fields['start_date'].widget.attrs['disabled'] = True
        courseForm.fields['end_date'].widget.attrs['disabled'] = True
        courseForm.fields['localization'].widget.attrs['disabled'] = True
        shipmentFormsAndNotebookTables = dict()
        for shipment in shipments:
#             ntbks = []
#             for ntbk in shipment.notebooks.all():
#                 ntbk.status = Notebook.#nasty, nasty, filthy hack not where it belongs.
#                 #@Future me: if you can process notebooks status on level of all() - 
#                 #somewhere in ManyRelatedManager - please do.
#                 ntbks.append(ntbk)
            notebookTable = NotebookTableWOStatus(shipment.notebooks.all())
            shipmentForm = addShipmentForm(instance=shipment,
                                                           initial={ 'shipment_id' : shipment.id,})
            shipmentForm.fields['shipment_from'].widget.attrs['disabled'] = True
            shipmentForm.fields['shipment_to'].widget.attrs['disabled'] = True
            shipmentFormsAndNotebookTables[shipmentForm] = notebookTable
        try:
            cs = course.notebooks.filter(localization = course.localization)
            localNotebooks = NotebookTableWOStatus(cs)
        except Notebook.DoesNotExist:
            localNotebooks = None
                     
        return render(request, 'addShipmentToCourse.html', {'courseForm': courseForm,
                                                                'shipmentFormsAndNotebooks': shipmentFormsAndNotebookTables,
                                                                'localNotebooks':localNotebooks,
                                                                })
        
        
def viewShipments(request):
    shipmentTable = ShipmentTable(Shipment.objects.all())
    return render(request, 'viewShipments.html', {'shipments': shipmentTable})


def deleteShipment(request, shipment_id=None):
    uri = request.META.get('HTTP_REFERER') or '/shipment/view/'
    shipment = get_or_none(Shipment, pk=shipment_id)
    if shipment != None:
        shipment.delete()
    return redirect(uri)


def editShipment(request, shipment_id=None):
    if request.method == "POST":
#         shipment_id = int(request.POST['shipment_id'].partition('"')[0])
        print shipment_id
        shipment_instance = get_object_or_404(Shipment, id=shipment_id)
        POST = request.POST.copy()  #That's fuckin ridicoulus workaround.
        #Trzeba zhackowac POSTa, te dwa pola są zablokowane i nie idą w requeście i formularz sie nie waliduje.
        POST['shipment_from'] = shipment_instance.shipment_from.id
        POST['shipment_to'] = shipment_instance.shipment_to.id
        sf = addShipmentForm(POST, instance=shipment_instance) #But it's working as hell
        if sf.is_valid():
            sf.save()
    else:
        print shipment_id
        shipment_instance = get_object_or_404(Shipment, id=shipment_id)
        shipmentForm = addShipmentForm(instance = shipment_instance)
        shipmentForm.fields['shipment_from'].widget.attrs['disabled'] = True
        shipmentForm.fields['shipment_to'].widget.attrs['disabled'] = True
        
        notebookTable = NotebookTable(shipment_instance.notebooks.all())
        
        return render(request, 'addShipment.html', {'shipmentForm': shipmentForm,
                                                        'notebookTable': notebookTable,
                                                        })
        
def viewCoursesAndShipmentsWeeks(request):
    def getCourseTableOrNone(list):
        if list.__len__() > 0:
            return CourseTable(list)
        return None
    
    coursesList = []
    for i in xrange(-1,3):
        courses = {}
        monday = weekday(datetime.date.today()+i*datetime.timedelta(7), 0)
        friday = weekday(datetime.date.today()+i*datetime.timedelta(7), 4)
        today = datetime.date.today()
        coursesInWeek = getCoursesListBetweenDates(monday, friday)
        courses['monday'] = monday
        courses['friday'] = friday
        courses['finished'] = getCourseTableOrNone([c for c in coursesInWeek if c.end_date <= today])
        courses['ongoing'] = getCourseTableOrNone([c for c in coursesInWeek if c.start_date <= today 
                                          and c.end_date > today])
        courses['incoming'] = getCourseTableOrNone([c for c in coursesInWeek if c.start_date > today])
        courses['dashTable'] = NotebookDashTable(getNotebooksListBetweenDates(monday, friday)).getHtml()
        coursesList.append(courses)
        print courses
    return render(request,'weekCoursesAndShipments.html', {'coursesMotherFuckinShip': coursesList})
        
        
    
