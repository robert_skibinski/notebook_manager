'''
Created on 20-06-2013

@author: bambo
'''
from django import forms
from django.contrib.admin import widgets
from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.localflavor.generic.forms import DateField
from django.forms.extras.widgets import SelectDateWidget
from notebooks_manager.models import Notebook, Place, Course, Shipment
import datetime

class DateForm(forms.Form):
    date = forms.DateField(widget=SelectDateWidget(years=[y for y in range(2012,2020)]))
    date1 = forms.DateField(widget=SelectDateWidget(years=[y for y in range(2012, 2020)]))
    
        
        
class addNotebookForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(addNotebookForm, self).__init__(*args, **kwargs)
        self.fields['notes'].required = False
        self.fields['notes'].default = ""
    class Meta:
        model= Notebook
    

class addCourseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(addCourseForm, self).__init__(*args, **kwargs)
        self.fields['notes'].required = False
        self.fields['notes'].default = ""
    course_id = forms.CharField(widget=forms.HiddenInput, required=False)
    class Meta:
        model = Course
        exclude = ('shipments', 'notebooks')
        widgets = {
                   'start_date' : SelectDateWidget(),
                   'end_date' : SelectDateWidget(),
                   }
        
class addShipmentForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(addShipmentForm, self).__init__(*args, **kwargs)
        self.fields['notes'].required = False
        self.fields['notes'].default = ""
    shipment_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    
    class Meta:
        model = Shipment
        widgets = {
                   'shipment_date' : SelectDateWidget(),
                   'delivery_date' : SelectDateWidget(),}
        exclude = ('notebooks',)
        