'''
Created on 18-06-2013

@author: bambo
'''
from django.db import models
from django.db.models.signals import pre_delete, post_save
from django.dispatch.dispatcher import receiver
class Place(models.Model):
    name = models.CharField(max_length=64, verbose_name='place')
    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name   
    
    
class Notebook(models.Model):
    LIVE = 1
    BROKEN = 2
    TRASHED = 3
    WORKING_STATUS = (
                      (LIVE, 'live'),
                      (BROKEN, 'broken'),
                      (TRASHED, 'trashed'),
                      )
    
    FREE = 1
    COURSE = 2
    TRENER = 3
    LENT = 4
    RESERVED = 5
    SHIPMENT = 6
    STATUS_CHOICE = (
                     (FREE, 'free'),
                     (COURSE,'in course'),
                     (TRENER, 'trener'),
                     (LENT, 'lent'),
                     (RESERVED, 'reserved'),
                     (SHIPMENT, 'shipment')
                     )
    brand = models.CharField(max_length=64, verbose_name='brand')
    model = models.CharField(max_length=64, verbose_name="Model")
    localization = models.ForeignKey(Place, verbose_name='Place')
    serial = models.CharField(max_length=64, verbose_name="Serial no.")
    working = models.IntegerField(choices = WORKING_STATUS, default = LIVE)
    status = models.IntegerField(choices = STATUS_CHOICE, default = FREE)
    notes = models.CharField(max_length=512, verbose_name="Notes", null=True)
    
    
    
    def __unicode__(self):
        return "<Notebook m:%s s:%s l:%s w:%s s:%s n:%s>" % (self.model, self.serial, self.localization,
                                                             self.working, self.status, self.notes)

class Shipment(models.Model):
    READY = 1
    IN_PROGRESS = 2
    FINISHED = 3
    DELAYED = 4
    NOT_PREPARED = 5
    STATUS = (
              (NOT_PREPARED, 'not prepared'),
              (READY, 'ready'),
              (IN_PROGRESS, 'in progress'),
              (FINISHED, 'finished'),
              (DELAYED, 'delayed/broken/something went terribly wrong'),
         )
    shipment_date = models.DateField(verbose_name = "designed shipment date")
    delivery_date = models.DateField(verbose_name = 'designed delivery date')
    shipment_from = models.ForeignKey(Place, verbose_name="shipment from", related_name="place_from")
    shipment_to = models.ForeignKey(Place, verbose_name="shipment to", related_name="place_to")
    shipment_status = models.IntegerField(choices = STATUS, default = NOT_PREPARED)
    notes = models.CharField(max_length=2048, verbose_name="notes", blank=True,null=True)
    notebooks = models.ManyToManyField(Notebook, verbose_name="notebooks", blank=True, null=True)
    
    def __unicode__(self):
        return "<Shipment id:%s | %s -> %s " %(self.id, self.shipment_from, self.shipment_to)
    
    class Meta:
        ordering = ['-shipment_date',]

    
class Course(models.Model):
    NOT_CONFIRMED = 1
    CONFIRMED = 2
    STATUS = (
              (NOT_CONFIRMED, 'not confirmed'),
              (CONFIRMED, 'confirmed'),
              )
    
    name = models.CharField(max_length=64, verbose_name='course name')
    start_date = models.DateField('start date')
    end_date = models.DateField('end field')
    localization = models.ForeignKey(Place, verbose_name="place")
    shipments = models.ManyToManyField(Shipment, verbose_name="shipments", null=True, default=None)
    notebooks = models.ManyToManyField(Notebook, verbose_name='notebooks', null=True, default=None)
    participants = models.IntegerField()
    notes = models.CharField(max_length=512, verbose_name="notes", null=True)
    course_status = models.IntegerField(choices = STATUS, default=NOT_CONFIRMED) 
    def __unicode__(self):
        return "<Course n:%s, sd:%s, ed:%s, p:%s, n%s>" %(self.name,
                                                                      self.start_date, self.end_date, self.participants, self.notes)
  
  
@receiver(pre_delete, sender=Course)
def courseDeleteHandler(sender, **kwargs):
    """
    Sets notebooks states assigned to the course again on free after the course had been deleted.
    """
    course_instance = kwargs['instance']
    for notebook in course_instance.notebooks.all():
        notebook.status = Notebook.FREE
        notebook.save()
    for shipment in course_instance.shipments.all():
        shipment.delete()
        
@receiver(post_save, sender=Shipment)
def updateNotebookPlase(sender, **kwargs):
    """
    Update Notebook place of being
    """
    shipment_instance = kwargs['instance']
    
    for notebook in shipment_instance.notebooks.all():
        if shipment_instance.shipment_status == Shipment.FINISHED:
            notebook.localization = shipment_instance.shipment_to
        if shipment_instance.shipment_status == Shipment.NOT_PREPARED:
            notebook.localization = shipment_instance.shipment_from;
        notebook.save()
    
    