# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'notebooks_manager.course': {
            'Meta': {'object_name': 'Course'},
            'course_status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notebooks': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['notebooks_manager.Notebook']", 'null': 'True', 'symmetrical': 'False'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'participants': ('django.db.models.fields.IntegerField', [], {}),
            'shipments': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['notebooks_manager.Shipment']", 'null': 'True', 'symmetrical': 'False'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'notebooks_manager.notebook': {
            'Meta': {'object_name': 'Notebook'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'serial': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'working': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'notebooks_manager.place': {
            'Meta': {'object_name': 'Place'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'notebooks_manager.shipment': {
            'Meta': {'ordering': "['-shipment_date']", 'object_name': 'Shipment'},
            'delivery_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notebooks': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['notebooks_manager.Notebook']", 'null': 'True', 'blank': 'True'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True'}),
            'shipment_date': ('django.db.models.fields.DateField', [], {}),
            'shipment_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'place_from'", 'to': u"orm['notebooks_manager.Place']"}),
            'shipment_status': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'shipment_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'place_to'", 'to': u"orm['notebooks_manager.Place']"})
        }
    }

    complete_apps = ['notebooks_manager']
    symmetrical = True
