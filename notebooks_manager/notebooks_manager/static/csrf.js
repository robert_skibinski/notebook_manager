$(document).ready(function(){ 
$.ajaxSetup({beforeSend: function(xhr, settings){
     xhr.setRequestHeader('X-CSRFToken', 
                          '{{ csrf_token }}');
   }});
});

function toggle(source) {
    checkboxes = document.getElementsByName('selection');
    for(var i in checkboxes)
        checkboxes[i].checked = source.checked;
}
