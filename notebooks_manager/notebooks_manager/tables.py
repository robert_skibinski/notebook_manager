#coding: utf-8
'''
Created on 12-07-2013

@author: bambo
'''
from cgi import escape

from django.utils.safestring import mark_safe
from django_tables2 import tables
from django_tables2.columns import CheckBoxColumn, TemplateColumn
from django_tables2.columns.base import Column
from django_tables2.utils import A
from collections import defaultdict

from notebooks_manager.models import Notebook, Shipment, Course


class NotebookTable(tables.Table):
    selection = CheckBoxColumn(accessor="pk", orderable= False, attrs = { "th__input": 
                                        {"id": "selectall"}})
    class Meta:
        model = Notebook
        attrs = {'class' : 'paleblue', 'id' : 'notebook_table'}
        orderable = False

#For shipment edit purpose, with utils column        
class NotebookEditTable(tables.Table):
    utils = TemplateColumn('<a href="/notebook/edit/{{record.id}}">Edit</a> | <a href="/notebook/delete/{{record.id}}">Delete</a>')
    class Meta:
        model = Notebook
        attrs = {'class' : 'paleblue', 'id' : 'notebook_table'}
        
class NotebookTableWOStatus(NotebookTable):#without status column
    class Meta(NotebookTable.Meta):
        exclude = ("status",)
        
class ShipmentTable(tables.Table):
    courseColumn = TemplateColumn('<a href="/course/edit/{{record.course_set.all.0.id}}/">{{record.course_set.all.0.name}}</a>')
    utils = TemplateColumn('<a href="/shipment/edit/{{record.id}}">Edit</a> | <a href="/shipment/delete/{{record.id}}">Delete</a>')
    class Meta:
        model = Shipment
        attrs = {'class': 'paleblue', 'id': 'shipments_table'}     
          
        
class CourseTable(tables.Table):
    
    utils = TemplateColumn('<a href="/course/edit/{{record.id}}">Edit</a> | <a href="/course/delete/{{record.id}}">Delete</a>')
    class Meta:
        model = Course
        attrs = {'class': 'paleblue', 'id': 'courses_table'}
        
class FinishedCourseTable(CourseTable):
    class confirmedColumn(Column):
        def render(self, record):
            if record.course_status == Course.CONFIRMED:
                return "Confirmed"
            elif record.course_status == Course.NOT_CONFIRMED:
                return mark_safe('<a href="/course/confirm/%d">Confirm end</a>' % record.id)
        
    confirmed = confirmedColumn(empty_values=())
    
    class Meta(CourseTable.Meta):
        pass
    
class NotebookDashTable():
    
    place = Column()
    freeNotebooks = Column()
    allNotebooks = Column()
    
    def __init__(self,notebooks, *args, **kwargs):
#         chuj = defaultdict(lambda: defaultdict(lambda: {'free':0, 'in use':0}))
        chuj={}
        for n in notebooks:
            if n.localization not in chuj:
                chuj[n.localization] = {"free": 0, "in use": 0}
            if n.status == Notebook.FREE:
                chuj[n.localization]['free'] += 1
            else:
                chuj[n.localization]['in use'] += 1

        self.ntable = "<table id=\"dash_table\"><tr><td>Miejsce</td><td>Zajęte</td><td>Wolne</td></tr>"
        for k,place in chuj.iteritems():
            self.ntable += "<tr><td>" + str(k) + "</td><td>" + str(place['free']) + "</td><td>" + str(place['in use']) + "</td></tr>"
        self.ntable += "</table>"
        
    def getHtml(self):
        return self.ntable
            
                
            
                
            
            
