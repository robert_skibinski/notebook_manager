'''
Created on 21-06-2013

@author: bambo
'''
from django.contrib import admin
from notebooks_manager.models import Notebook, Place, Course, Shipment

admin.site.register(Notebook)
admin.site.register(Place)
admin.site.register(Course)
admin.site.register(Shipment)
