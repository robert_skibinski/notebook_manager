from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'notebooks_manager.views.home', name='home'),
    # url(r'^notebooks_manager/', include('notebooks_manager.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'notebooks_manager.views.viewCoursesAndShipmentsWeeks', name='home'),
    url(r'^notebook/new/$', 'notebooks_manager.views.addNotebook'),
    url(r'^notebook/view/$', 'notebooks_manager.views.viewNotebooks', name='notebook_view'),
    url(r'^notebook/edit/(?P<notebook_id>\d+)/$', 'notebooks_manager.views.addNotebook', name="notebook_edit"),
    url(r'^notebook/delete/(?P<notebook_od>\d+)/$', 'notebooks_manager.views.deleteNotebook'),
#     url(r'^notebook/success/(?P<notebookID>\d+)/$', 'notebooks_manager.views.addNotebookOnSuccess', name="notebook_new_success"),
    url(r'^shipment/new/$', 'notebooks_manager.views.addShipment', name="shipment_new"),
    url(r'^shipment/view/$', 'notebooks_manager.views.viewShipments', name='view_shipment'),
    url(r'^shipment/edit/(?P<shipment_id>\d+)/$', 'notebooks_manager.views.editShipment', name="edit_shipment"),
    url(r'^shipment/delete/(?P<shipment_id>\d+)/$', 'notebooks_manager.views.deleteShipment', name="delete_shipment"),
#     url(r'^shipment/success/(?P<shipmentID>\d+)/$', 'notebooks_manager.views.addShipmentOnSuccess', name="shipment_new_success"),
    url(r'^course/new/$', 'notebooks_manager.views.addCourse', name="course_new"),
    url(r'^course/edit/(?P<course_id>\d+)/$', 'notebooks_manager.views.addShipmentToCourse', name="course_edit"),
    url(r'^course/view/$', 'notebooks_manager.views.viewCourses', name="view_courses"),
    url(r'^course/confirm/(?P<course_id>\d+)/$', 'notebooks_manager.views.confirmCourse', name='confirm_course'),    
#     url(r'^course/addShipment/$', 'notebooks_manager.views.addShipment'),
    url(r'^course/addShipment/(?P<course_id>\d+)/$', 'notebooks_manager.views.addShipmentToCourse'),
    url(r'^course/delete/(?P<course_id>\d+)/$', 'notebooks_manager.views.deleteCourse'),
    url(r'^course/weekView/$', 'notebooks_manager.views.viewCoursesAndShipmentsWeeks')
#     url(r'^course/edit/(?P<courseID>\d+)#(?P<shipmentID>\d+)/$', 'notebooks_manager.views.editCourse', name="course_edit"),
#     url(r'^course/success/(?P<courseID>\d+)/$', 'notebooks.views.addCourseOnSuccess', name="course_new_success"),
#     url(r'^course/failure/$', 'notebooks.views.addCourseOnFailure', name="course_new_failure"),
)
