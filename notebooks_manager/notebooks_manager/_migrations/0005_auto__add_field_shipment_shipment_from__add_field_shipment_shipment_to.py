# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Shipment.shipment_from'
        db.add_column(u'notebooks_manager_shipment', 'shipment_from',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, related_name='place_from', to=orm['notebooks_manager.Place']),
                      keep_default=False)

        # Adding field 'Shipment.shipment_to'
        db.add_column(u'notebooks_manager_shipment', 'shipment_to',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, related_name='place_to', to=orm['notebooks_manager.Place']),
                      keep_default=False)

        # Adding M2M table for field notebooks on 'Shipment'
        m2m_table_name = db.shorten_name(u'notebooks_manager_shipment_notebooks')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('shipment', models.ForeignKey(orm[u'notebooks_manager.shipment'], null=False)),
            ('notebook', models.ForeignKey(orm[u'notebooks_manager.notebook'], null=False))
        ))
        db.create_unique(m2m_table_name, ['shipment_id', 'notebook_id'])


    def backwards(self, orm):
        # Deleting field 'Shipment.shipment_from'
        db.delete_column(u'notebooks_manager_shipment', 'shipment_from_id')

        # Deleting field 'Shipment.shipment_to'
        db.delete_column(u'notebooks_manager_shipment', 'shipment_to_id')

        # Removing M2M table for field notebooks on 'Shipment'
        db.delete_table(db.shorten_name(u'notebooks_manager_shipment_notebooks'))


    models = {
        u'notebooks_manager.course': {
            'Meta': {'object_name': 'Course'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notebooks': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['notebooks_manager.Notebook']", 'null': 'True', 'symmetrical': 'False'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'participants': ('django.db.models.fields.IntegerField', [], {}),
            'shipments': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['notebooks_manager.Shipment']", 'null': 'True', 'symmetrical': 'False'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'notebooks_manager.notebook': {
            'Meta': {'object_name': 'Notebook'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'serial': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'working': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'notebooks_manager.place': {
            'Meta': {'object_name': 'Place'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'notebooks_manager.shipment': {
            'Meta': {'object_name': 'Shipment'},
            'delivery_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notebooks': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['notebooks_manager.Notebook']", 'null': 'True', 'blank': 'True'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True'}),
            'shipment_date': ('django.db.models.fields.DateField', [], {}),
            'shipment_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'place_from'", 'to': u"orm['notebooks_manager.Place']"}),
            'shipment_status': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'shipment_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'place_to'", 'to': u"orm['notebooks_manager.Place']"})
        }
    }

    complete_apps = ['notebooks_manager']