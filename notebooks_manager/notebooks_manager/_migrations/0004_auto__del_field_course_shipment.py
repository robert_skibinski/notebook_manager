# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Course.shipment'
        db.delete_column(u'notebooks_manager_course', 'shipment_id')

        # Adding M2M table for field shipments on 'Course'
        m2m_table_name = db.shorten_name(u'notebooks_manager_course_shipments')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('course', models.ForeignKey(orm[u'notebooks_manager.course'], null=False)),
            ('shipment', models.ForeignKey(orm[u'notebooks_manager.shipment'], null=False))
        ))
        db.create_unique(m2m_table_name, ['course_id', 'shipment_id'])


    def backwards(self, orm):
        # Adding field 'Course.shipment'
        db.add_column(u'notebooks_manager_course', 'shipment',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['notebooks_manager.Shipment']),
                      keep_default=False)

        # Removing M2M table for field shipments on 'Course'
        db.delete_table(db.shorten_name(u'notebooks_manager_course_shipments'))


    models = {
        u'notebooks_manager.course': {
            'Meta': {'object_name': 'Course'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notebooks': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['notebooks_manager.Notebook']", 'null': 'True', 'symmetrical': 'False'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'participants': ('django.db.models.fields.IntegerField', [], {}),
            'shipments': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['notebooks_manager.Shipment']", 'null': 'True', 'symmetrical': 'False'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'notebooks_manager.notebook': {
            'Meta': {'object_name': 'Notebook'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'serial': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'working': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'notebooks_manager.place': {
            'Meta': {'object_name': 'Place'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'notebooks_manager.shipment': {
            'Meta': {'object_name': 'Shipment'},
            'delivery_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True'}),
            'shipment_date': ('django.db.models.fields.DateField', [], {}),
            'shipment_status': ('django.db.models.fields.IntegerField', [], {'default': '5'})
        }
    }

    complete_apps = ['notebooks_manager']