# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Course.localization'
        db.add_column(u'notebooks_manager_course', 'localization',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['notebooks_manager.Place']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Course.localization'
        db.delete_column(u'notebooks_manager_course', 'localization_id')


    models = {
        u'notebooks_manager.course': {
            'Meta': {'object_name': 'Course'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notebooks': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['notebooks_manager.Notebook']", 'symmetrical': 'False'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'participants': ('django.db.models.fields.IntegerField', [], {}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Shipment']"}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'notebooks_manager.notebook': {
            'Meta': {'object_name': 'Notebook'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'serial': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'working': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'notebooks_manager.place': {
            'Meta': {'object_name': 'Place'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'notebooks_manager.shipment': {
            'Meta': {'object_name': 'Shipment'},
            'delivery_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True'}),
            'shipment_date': ('django.db.models.fields.DateField', [], {}),
            'shipment_status': ('django.db.models.fields.IntegerField', [], {'default': '5'})
        }
    }

    complete_apps = ['notebooks_manager']