# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Place'
        db.create_table(u'notebooks_manager_place', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'notebooks_manager', ['Place'])

        # Adding model 'Notebook'
        db.create_table(u'notebooks_manager_notebook', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('model', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('localization', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['notebooks_manager.Place'])),
            ('serial', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('working', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('notes', self.gf('django.db.models.fields.CharField')(max_length=512, null=True)),
        ))
        db.send_create_signal(u'notebooks_manager', ['Notebook'])

        # Adding model 'Shipment'
        db.create_table(u'notebooks_manager_shipment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shipment_date', self.gf('django.db.models.fields.DateField')()),
            ('delivery_date', self.gf('django.db.models.fields.DateField')()),
            ('shipment_status', self.gf('django.db.models.fields.IntegerField')(default=5)),
            ('notes', self.gf('django.db.models.fields.CharField')(max_length=2048, null=True)),
        ))
        db.send_create_signal(u'notebooks_manager', ['Shipment'])

        # Adding model 'Course'
        db.create_table(u'notebooks_manager_course', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('shipment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['notebooks_manager.Shipment'])),
            ('participants', self.gf('django.db.models.fields.IntegerField')()),
            ('notes', self.gf('django.db.models.fields.CharField')(max_length=512, null=True)),
        ))
        db.send_create_signal(u'notebooks_manager', ['Course'])

        # Adding M2M table for field notebooks on 'Course'
        m2m_table_name = db.shorten_name(u'notebooks_manager_course_notebooks')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('course', models.ForeignKey(orm[u'notebooks_manager.course'], null=False)),
            ('notebook', models.ForeignKey(orm[u'notebooks_manager.notebook'], null=False))
        ))
        db.create_unique(m2m_table_name, ['course_id', 'notebook_id'])


    def backwards(self, orm):
        # Deleting model 'Place'
        db.delete_table(u'notebooks_manager_place')

        # Deleting model 'Notebook'
        db.delete_table(u'notebooks_manager_notebook')

        # Deleting model 'Shipment'
        db.delete_table(u'notebooks_manager_shipment')

        # Deleting model 'Course'
        db.delete_table(u'notebooks_manager_course')

        # Removing M2M table for field notebooks on 'Course'
        db.delete_table(db.shorten_name(u'notebooks_manager_course_notebooks'))


    models = {
        u'notebooks_manager.course': {
            'Meta': {'object_name': 'Course'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notebooks': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['notebooks_manager.Notebook']", 'symmetrical': 'False'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'participants': ('django.db.models.fields.IntegerField', [], {}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Shipment']"}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'notebooks_manager.notebook': {
            'Meta': {'object_name': 'Notebook'},
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localization': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notebooks_manager.Place']"}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'}),
            'serial': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'working': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'notebooks_manager.place': {
            'Meta': {'object_name': 'Place'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'notebooks_manager.shipment': {
            'Meta': {'object_name': 'Shipment'},
            'delivery_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'null': 'True'}),
            'shipment_date': ('django.db.models.fields.DateField', [], {}),
            'shipment_status': ('django.db.models.fields.IntegerField', [], {'default': '5'})
        }
    }

    complete_apps = ['notebooks_manager']