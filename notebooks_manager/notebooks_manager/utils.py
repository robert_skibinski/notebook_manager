import datetime

from django.db import models
from django.db.models.query_utils import Q

from notebooks_manager.models import Shipment, Course, Notebook


def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)
def weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    return d + datetime.timedelta(days_ahead)

def freeNotebooks(start_date, end_date, notebooksList):
	pass
	
def get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None
    except ValueError:
    	return None

def getNotebooksListOnDate(date):
    shipmentsOnDate = Shipment.objects.filter(shipment_date__lte=date, delivery_date__gte=date)
    coursesOnDate = Course.objects.filter(start_date__lte=date, end_date__gte=date)
    notebooks = set()
    for shipment in shipmentsOnDate:
        for notebook in shipment.notebooks.all():
            notebook.status = Notebook.SHIPMENT
            notebooks.add(notebook)
            
    for course in coursesOnDate:
        for notebook in course.notebooks.all():
            notebook.status = Notebook.COURSE
            notebooks.add(notebook)
            
    coursesOnDate = Course.objects.filter(start_date__gt=date)
    for course in coursesOnDate:
        for notebook in course.notebooks.all():
            notebook.status = Notebook.RESERVED
            notebooks.add(notebook)
            
    notebooks |= set(Notebook.objects.all())
    return notebooks

def getNotebooksListBetweenDates(date1, date2):
    shipmentsOnDate = Shipment.objects.filter(Q(shipment_date__lte=date1, delivery_date__gte=date2))
    coursesOnDate = getCoursesListBetweenDates(date1, date2)
    
    notebooks = set()
    for shipment in shipmentsOnDate:
        for notebook in shipment.notebooks.all():
            notebook.status = Notebook.SHIPMENT
            notebooks.add(notebook)
    
    for course in coursesOnDate:
        for notebook in course.notebooks.all():
            notebook.status = Notebook.COURSE
            notebook.localization = course.localization
            notebooks.add(notebook)
            
    notebooks |= set(Notebook.objects.all())
    return notebooks

def getCoursesListBetweenDates(date1,date2):
    return Course.objects.filter(Q(start_date__lte=date1, end_date__gte=date2) |
                                          Q(start_date__lte=date2, end_date__gte=date2) | 
                                          Q(start_date__lte=date1, end_date__gte=date1) | 
                                          Q(start_date__gte=date1, end_date__lte=date2)
                                          )

def dashboard():
    pass