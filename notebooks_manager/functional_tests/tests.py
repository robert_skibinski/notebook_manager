#!/home/bambo/virtualenvs/django/bin/python
#coding: utf-8
'''
Created on 18-06-2013

@author: bambo
'''
from django.test.testcases import LiveServerTestCase
from notebooks_manager.models import Notebook
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import random
import time


class addNotebookPageTests(LiveServerTestCase):
    '''
    Test functionality for adding notebooks to database via html POST form.
    '''
    def getAddNotebookFormFields(self):
        self.modelbox = self.browser.find_element_by_id('id_model')
        self.serialbox = self.browser.find_element_by_id('id_serial')
        self.localizationbox = self.browser.find_element_by_id('id_localization')
        self.submitbutton = self.browser.find_element_by_id('id_submit_new_notebook_button')
        
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url+"/addNotebook")
        self.browser.implicitly_wait(3)
        
    def tearDown(self):
        self.browser.quit()
        
    def test_CanAddNewNotebookToDatabase(self):
        self.getAddNotebookFormFields()
        
        self.modelbox.send_keys('Lenovo')
        self.serialbox.send_keys('081-940')
        self.localizationbox.send_keys(u'Kraków')
        self.submitbutton.send_keys(Keys.ENTER)
        
        confirmParagraph = self.browser.find_element_by_id("id_message_bar")
        self.assertIn('Lenovo', confirmParagraph.text)
        self.assertIn('081-940', confirmParagraph.text)
        self.assertIn(u'Kraków', confirmParagraph.text)
        nb = Notebook.objects.all()
        for n in nb:
            print str(n)
        
    def test_canAddTwoNotebooksToDatabase(self):
        self.getAddNotebookFormFields()
        
        self.modelbox.send_keys('Lenovo')
        self.serialbox.send_keys('081-940')
        self.localizationbox.send_keys(u'Kraków')
        self.submitbutton.send_keys(Keys.ENTER)      
        confirmParagraph = self.browser.find_element_by_id("id_message_bar")
        self.assertIn('Lenovo', confirmParagraph.text)
        self.assertIn('081-940', confirmParagraph.text)
        self.assertIn(u'Kraków', confirmParagraph.text)
        
        self.getAddNotebookFormFields()
        self.modelbox.send_keys('Dell')
        self.serialbox.send_keys('940-081')
        self.localizationbox.send_keys(u'Kraków')
        self.submitbutton.send_keys(Keys.ENTER)      
        confirmParagraph = self.browser.find_element_by_id("id_message_bar")
        self.assertIn('Dell', confirmParagraph.text)
        self.assertIn('940-081', confirmParagraph.text)
        self.assertIn(u'Kraków', confirmParagraph.text)     
 
class viewNotebookPageTests(LiveServerTestCase):
    def addNotebook(self, count = 1):
        for i in xrange(count):
            ntbk = Notebook()
            ntbk.model = '%010x' % random.randrange(256**15)
            ntbk.serial = '%08x' % random.randrange(256**15)
            ntbk.localization = '%015x' % random.randrange(256**15)
            ntbk.save()
            
    def setUp(self):
        self.addNotebook(10)
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url+"/viewNotebooks")
     
    def tearDown(self):
        self.browser.quit()
         

        
             

        
        
        
        
        